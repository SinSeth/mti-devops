python3 -m venv .venv
source .venv/bin/activate
python -m pip install -r requirements.txt

uvicorn app.main:app --reload

deactivate
