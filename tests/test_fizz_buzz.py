from app.service.fizz_buzz import fizz_buzz


def test_fizz_buzz_of_1():
	assert fizz_buzz(1) == "1"


def test_fizz_buzz_of_3():
	assert fizz_buzz(3) == "fizz"


def test_fizz_buzz_of_6():
	assert fizz_buzz(6) == "fizz"


def test_fizz_buzz_of_9():
	assert fizz_buzz(9) == "fizz"


def test_fizz_buzz_of_5():
	assert fizz_buzz(5) == "buzz"


def test_fizz_buzz_of_10():
	assert fizz_buzz(10) == "buzz"


def test_fizz_buzz_of_20():
	assert fizz_buzz(20) == "buzz"


def test_fizz_buzz_of_15():
	assert fizz_buzz(15) == "fizzbuzz"


def test_fizz_buzz_of_30():
	assert fizz_buzz(30) == "fizzbuzz"


def test_fizz_buzz_of_45():
	assert fizz_buzz(45) == "fizzbuzz"