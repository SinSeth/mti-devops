from fastapi import FastAPI
from pydantic import BaseModel
from app.service.fizz_buzz import *
import uvicorn

app = FastAPI()


class FizzBuzzInput(BaseModel):
    value: int


@app.post("/")
def post_fizz_buzz(fizz_buzz_input: FizzBuzzInput):
    return fizz_buzz(fizz_buzz_input.value)


@app.get("/")
def test():
    return {'message': 'Hello Dilam !'}


if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
